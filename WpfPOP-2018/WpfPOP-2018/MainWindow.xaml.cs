﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnAerodromi_Click(object sender, RoutedEventArgs e)
        {
            Aerodromi a = new Aerodromi();
            a.Show();
        }

        private void BtnLetovi_Click(object sender, RoutedEventArgs e)
        {
            Letovi l = new Letovi();
            l.Show();
        }

        private void Admin_Click(object sender, RoutedEventArgs e)
        {
            AvioniW a = new AvioniW();
            a.Show();
        }

        private void Aviokompanije_Click(object sender, RoutedEventArgs e)
        {
            AviokompanijeW avio = new AviokompanijeW();
            avio.Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            GlavniW g = new GlavniW();
            g.Show();
        }

        private void BtnKorisnici_Click(object sender, RoutedEventArgs e)
        {
            KorisniciW kori = new KorisniciW();
            kori.Show();
        }

        private void BtnKarte_Click(object sender, RoutedEventArgs e)
        {
            KarteW w = new KarteW();
            w.Show();
        }

        private void BtnSedista_Click(object sender, RoutedEventArgs e)
        {
            SedistaW n = new SedistaW();
            n.Show();

        }

        private void BtnNalog_Click(object sender, RoutedEventArgs e)
        {
            LogovanjeW m = new LogovanjeW();
            m.Show();
        }
    }
}
