﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for LogovanjeW.xaml
    /// </summary>
    public partial class LogovanjeW : Window
    {
        public LogovanjeW()
        {
            InitializeComponent();
        }

        private void BtnZatvor_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            foreach(Korisnik korisnik in Data.Instance.korisnici)
            {
                if(txtKorisnicko.Text.Equals(korisnik.KorisnickoIme) && txtSifra.Password.Equals(korisnik.Lozinka))
                {
                    NalogW n = new NalogW(korisnik);
                    Data.Instance.Ulogova = korisnik.KorisnickoIme;
                    MessageBox.Show(Data.Instance.Ulogova);
                    n.Show();
                        
                }
               
                
            }
            
        }
    }
}
