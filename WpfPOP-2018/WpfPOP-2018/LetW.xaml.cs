﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for Letovi.xaml
    /// </summary>
    public partial class Letovi : Window
    {
        ICollectionView view;
        private Aviokompanija aviokompanija;
        public Letovi()
        {
           
            InitializeComponent();
           
            view = CollectionViewSource.GetDefaultView(Data.Instance.let);
            DGLetovi.ItemsSource = Data.Instance.let;

            view.Filter = CustomFilter;
           
            DGLetovi.IsSynchronizedWithCurrentItem = true;
            DGLetovi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            DGLetovi.SelectedIndex = 0;

            foreach (Korisnik korisnik in Data.Instance.korisnici)
            {
                if (korisnik.KorisnickoIme.Equals(Data.Instance.Ulogova))
                {
                    BtnDodaj.Visibility = Visibility.Hidden;
                    BtnIzmeni.Visibility = Visibility.Hidden;
                    BtnObrisi.Visibility = Visibility.Hidden;
                }
            
            }

        }
        private bool CustomFilter(object obj)
        {
            Let let = obj as Let;
            if (txtSearch.Equals(String.Empty))
            {
                return let.Aktivan;
            }
            else
            {
                return let.Aktivan && let.Sifra.Contains(txtSearch.Text) || let.Destinacija.Contains(txtSearch.Text) || let.Odrediste.Contains(txtSearch.Text);
            }
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            EditLet edit = new EditLet(new Let(),aviokompanija);
            edit.ShowDialog();
            DGLetovi.Items.Refresh();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Let selektovanLet = (Let)DGLetovi.SelectedItem;
            if (selektovanLet != null)
            {
                Let stari = (Let)selektovanLet.Clone();
                EditLet wLet = new EditLet(selektovanLet,aviokompanija, EditLet.Opcija.izmena);
                if (wLet.ShowDialog() != true || validacija(selektovanLet)==false )
                {
                    int indeks = IndexLet(selektovanLet.Sifra);
                    Data.Instance.let[indeks] = stari;
                }

            }
            else
            {
                MessageBox.Show("Nije selektovan nijedan let");
            }
            selektovanLet.izmena();
            DGLetovi.Items.Refresh();
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Let let = (Let)DGLetovi.SelectedItem;

            if (SelektovanLet(let))
            {
                if (MessageBox.Show("Da li ste sigurni", "Potvrda", MessageBoxButton.YesNoCancel).Equals(MessageBoxResult.Yes))
                {
                    var index = IndexLet(let.Sifra);
                    Data.Instance.let[index].Aktivan = false;
                    Let selektovan = (Let)DGLetovi.SelectedItem;
                    selektovan.izmena();
                  

                }
            }
            view.Refresh();


        }
        private int IndexLet(String let)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.let.Count; i++)
            {
                if (Data.Instance.let[i].Sifra.Equals(let))
                {
                    index = i;

                }

            }
            return index;

        }
        private bool SelektovanLet(Let let)
        {
            if (let == null)
            {
                MessageBox.Show("Morate izabrati let");
                return false;
            }
            return true;
        }
       private bool validacija(Let let)
        {
            if(let.Cena <= 0)
            {
                MessageBox.Show("Cena mora biti veca od 0");
                return false;
            }
            try
            {
                Convert.ToDouble(let.Cena);
            }
            catch(Exception e)
            {
                MessageBox.Show("Cena mora biti broj"+e.Message);
                return false;
                
            }
            if (let.Odrediste.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti odrediste");
                return false;
            }
            if (let.Destinacija.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti destinaciju");
                return false;
            }
            if (let.VremeDolaska.Equals(string.Empty))
            {
                MessageBox.Show("Morate vreme dolaska");
                return false;
            }
            if (let.VremePolaska.Equals(string.Empty))
            {
                MessageBox.Show("Morate vreme polaska");
                return false;
            }

            return true;
        }



        private void BtnIme_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("Sifra", ListSortDirection.Ascending));
        }

        private void BtnPolazak_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("Destinacija", ListSortDirection.Ascending));
        }

        private void BtnDolazak_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("Cena", ListSortDirection.Descending));
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void BtnKarta_Click(object sender, RoutedEventArgs e)
        {
            
            Let selektovanLet = (Let)DGLetovi.SelectedItem;
            if (selektovanLet != null)
            {
                
                BiznisW wLet = new BiznisW(selektovanLet,new Karta());
                wLet.ShowDialog();
              
            }
            else
            {
                MessageBox.Show("Morate izabrati za koji let kupujete kartu");
            }
        }
        private void dgFakulteti_AutoGeneratingColumn(object sender,
          DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Aktivan" || (string)e.Column.Header == "Lista" || (string)e.Column.Header == "Lista2")
            {
                e.Cancel = true;
            }
        }
    }
}
