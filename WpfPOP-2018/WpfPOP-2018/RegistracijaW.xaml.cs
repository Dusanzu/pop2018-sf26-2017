﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for RegistracijaW.xaml
    /// </summary>
    public partial class RegistracijaW : Window
    {
        public enum Opcija { Dodavanje, Izmena, Registracija }
        private Opcija opcija;
        private Korisnik korisnik;
        public RegistracijaW(Korisnik korisnik, Opcija opcija = Opcija.Dodavanje)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            this.opcija = opcija;

            this.DataContext = korisnik;

            cbPol.ItemsSource = Data.Instance.korisnici.Select(a => a.Pol);

            cbTip.ItemsSource = Data.Instance.korisnici.Select(a => a.TipKorisnka);
            if (opcija.Equals(Opcija.Registracija))
            {
                korisnik.TipKorisnka = "Putnik";
                cbTip.Visibility = Visibility.Hidden;
                
            }

            if (opcija.Equals(Opcija.Izmena))
            {
                txtKorisnicko.IsReadOnly = true;

            }
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
           
            if (opcija.Equals(Opcija.Registracija) && validacija() == true)
            {
                korisnik.Aktivan = true;
                korisnik.UnosUBazu();
                NalogW k = new NalogW(korisnik);
                k.Show();

            }

        }
        public bool validacija()
        {
            foreach (Korisnik aviokompanija in Data.Instance.korisnici)
            {
                if (aviokompanija.KorisnickoIme.Equals(txtKorisnicko.Text))
                {
                    MessageBox.Show("Korisnicko vec postoji");
                    return false;
                }
            }
            if (string.Empty.Equals(txtKorisnicko.Text))
            {
                MessageBox.Show("Korisnicko nije uneto");
                return false;
            }
            if (txtIme.Text.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti ime");
                return false;
            }
            if (txtPrezime.Text.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti prezime");
                return false;
            }
            if (txtAdresa.Text.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti adresu");
                return false;
            }
            if (cbPol.SelectedItem == null)
            {
                MessageBox.Show("Morate uneti pol");
                return false;
            }
          
            return true;
        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
