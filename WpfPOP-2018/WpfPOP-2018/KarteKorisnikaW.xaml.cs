﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for KarteKorisnikaW.xaml
    /// </summary>
    public partial class KarteKorisnikaW : Window
    {
        private ICollectionView view;
        public List<Karta> karte = new List<Karta>();
        public KarteKorisnikaW()
        {
            
            InitializeComponent();

            view = CollectionViewSource.GetDefaultView(karte);
            view.Filter = CustomFilter;

            foreach (Karta korisnik in Data.Instance.karte)
            {
                if (korisnik.Putnik.Equals(Data.Instance.Ulogova) && korisnik.Aktivan==true) {
                    karte.Add(korisnik);
                }
               
            }




            DGKarte.ItemsSource =karte;
            DGKarte.IsSynchronizedWithCurrentItem = true;
            DGKarte.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private bool CustomFilter(object obj)
        {
            Karta aerodrom = obj as Karta;
           

            if (TxtIme.Equals(String.Empty))
            {
                return aerodrom.Aktivan;
            }
            else
            {
                return aerodrom.Aktivan && aerodrom.Putnik.Contains(TxtIme.Text);
            }

        }
        private void DGKarte_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void dgFakulteti_AutoGeneratingColumn(object sender,
      DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Aktivan" /*|| (string)e.Column.Header == "Aktivan"*/)
            {
                e.Cancel = true;
            }
        }
        private void TxtIme_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
