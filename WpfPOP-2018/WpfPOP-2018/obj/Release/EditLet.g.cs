﻿#pragma checksum "..\..\EditLet.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "61B8EFA37574E58C1FDC83F1361BC8FBFE019C63"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfPOP_2018;


namespace WpfPOP_2018 {
    
    
    /// <summary>
    /// EditLet
    /// </summary>
    public partial class EditLet : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\EditLet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblSifra;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\EditLet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtSifra;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\EditLet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblDestinacija;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\EditLet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbPolaziste;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\EditLet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblOdrediste;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\EditLet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbOdrediste;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\EditLet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblPolazak;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\EditLet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpPolazak;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\EditLet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblDolazak;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\EditLet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpDolazak;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\EditLet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblCena;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\EditLet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtCena;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\EditLet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Odustani;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\EditLet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Sacuvaj;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfPOP-2018;component/editlet.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\EditLet.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.LblSifra = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.TxtSifra = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.LblDestinacija = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.cbPolaziste = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 5:
            this.LblOdrediste = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.cbOdrediste = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.LblPolazak = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.dpPolazak = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 9:
            this.LblDolazak = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.dpDolazak = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 11:
            this.LblCena = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.TxtCena = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.Odustani = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\EditLet.xaml"
            this.Odustani.Click += new System.Windows.RoutedEventHandler(this.Odustani_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.Sacuvaj = ((System.Windows.Controls.Button)(target));
            
            #line 36 "..\..\EditLet.xaml"
            this.Sacuvaj.Click += new System.Windows.RoutedEventHandler(this.Sacuvaj_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

