﻿#pragma checksum "..\..\IzmenaKarteW.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "F9169E1E776A6830C90304CE72D1B41E4BDC71E2"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfPOP_2018;


namespace WpfPOP_2018 {
    
    
    /// <summary>
    /// IzmenaKarteW
    /// </summary>
    public partial class IzmenaKarteW : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\IzmenaKarteW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblBrojLeta;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\IzmenaKarteW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbBrojLeta;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\IzmenaKarteW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblKlasa;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\IzmenaKarteW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbKlasa;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\IzmenaKarteW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnDodaj;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\IzmenaKarteW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnOtkazi;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfPOP-2018;component/izmenakartew.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\IzmenaKarteW.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.lblBrojLeta = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.cbBrojLeta = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 3:
            this.lblKlasa = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.cbKlasa = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 5:
            this.BtnDodaj = ((System.Windows.Controls.Button)(target));
            
            #line 20 "..\..\IzmenaKarteW.xaml"
            this.BtnDodaj.Click += new System.Windows.RoutedEventHandler(this.BtnDodaj_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.BtnOtkazi = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\IzmenaKarteW.xaml"
            this.BtnOtkazi.Click += new System.Windows.RoutedEventHandler(this.BtnOtkazi_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

