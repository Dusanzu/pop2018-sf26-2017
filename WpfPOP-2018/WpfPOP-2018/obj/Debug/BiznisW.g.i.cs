﻿#pragma checksum "..\..\BiznisW.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "D65FEFB4594AC3022AE7F972E06BCB944EB53DA4"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfPOP_2018;


namespace WpfPOP_2018 {
    
    
    /// <summary>
    /// BiznisW
    /// </summary>
    public partial class BiznisW : System.Windows.Window, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 27 "..\..\BiznisW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtBlock;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\BiznisW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl lst;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\BiznisW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl lst2;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\BiznisW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnDodaj;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\BiznisW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnOtkazi;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\BiznisW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblPutnik;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\BiznisW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPutnik;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\BiznisW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBiznis;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\BiznisW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEkonomska;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\BiznisW.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbKlasa;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfPOP-2018;component/biznisw.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\BiznisW.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 2:
            this.txtBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.lst = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 4:
            this.lst2 = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 5:
            this.BtnDodaj = ((System.Windows.Controls.Button)(target));
            
            #line 42 "..\..\BiznisW.xaml"
            this.BtnDodaj.Click += new System.Windows.RoutedEventHandler(this.BtnDodaj_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.BtnOtkazi = ((System.Windows.Controls.Button)(target));
            
            #line 43 "..\..\BiznisW.xaml"
            this.BtnOtkazi.Click += new System.Windows.RoutedEventHandler(this.BtnOtkazi_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.lblPutnik = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.txtPutnik = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.btnBiznis = ((System.Windows.Controls.Button)(target));
            
            #line 46 "..\..\BiznisW.xaml"
            this.btnBiznis.Click += new System.Windows.RoutedEventHandler(this.BtnBiznis_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.btnEkonomska = ((System.Windows.Controls.Button)(target));
            
            #line 47 "..\..\BiznisW.xaml"
            this.btnEkonomska.Click += new System.Windows.RoutedEventHandler(this.BtnEkonomska_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.lbKlasa = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 11 "..\..\BiznisW.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

