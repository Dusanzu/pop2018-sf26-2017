﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WpfPOP_2018.Model;

namespace WpfPOP_2018.Database
{
    class Data
    {
        // public List<Korisnici> kori { get; set; }
       
        public ObservableCollection<Let> let { get; set; }
      
        public ObservableCollection<Korisnik> korisnici { get; set; }
        public ObservableCollection<Karta> karte { get; set; }
       public  ObservableCollection<Aerodrom> aerodrom1 { get; set; }
        public ObservableCollection<Avion> avioni { get; set; }
        public ObservableCollection<Aviokompanija> aviokompanije { get; set; }
       public ObservableCollection<Let> letovi { get; set; } 

        public List<Sedista> sedista { get; set; }
        //public List<Letovi> let { get; set; }
        public string Ulogova { get; set; }




        public Data()
        {
            
            Ulogova = string.Empty;
            //      kori = new List<Korisnici>();
            aerodrom1=new ObservableCollection<Aerodrom>();
            let = new ObservableCollection<Let>();
            korisnici = new ObservableCollection<Korisnik>();
            karte = new ObservableCollection<Karta>();
           
            avioni = new ObservableCollection<Avion>();
            aviokompanije = new ObservableCollection<Aviokompanija>();
            letovi = new ObservableCollection<Let>();
            sedista = new List<Sedista>();


            
            IspisIzLetova();
            
            IspisIzKorisnika();
            IspisIzUBazeAerodroma();
            IspisIzAviokompanija();
            IspisIzKarata();
           
            IspisIzUBazeAviona();
           
        }

        public void IspisIzUBazeAviona()
        {
            avioni.Clear();

            using (SqlConnection conn = new SqlConnection())
            {


                conn.ConnectionString = CONNECTION;
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM Avioni";
                SqlDataAdapter daAerodromi = new SqlDataAdapter();
                DataSet dsAerodromi = new DataSet();
                daAerodromi.SelectCommand = command;
                daAerodromi.Fill(dsAerodromi, "Avioni");
                foreach (DataRow row in dsAerodromi.Tables["Avioni"].Rows)
                {
                    Avion aerodrom = new Avion();
                    // aerodrom. = (int)row["id"];
                    aerodrom.Sifra = (string)row["Sifra"];
                    aerodrom.Pilot = (string)row["Pilot"];
                    aerodrom.BrojLeta = (string)row["BrojLeta"];
                    aerodrom.NazivAviokompanije = (string)row["NazivAviokompanije"];
                  
                    aerodrom.Aktivan = (bool)row["Aktivan"];
                    avioni.Add(aerodrom);
                }

            }

        }

        public void IspisIzUBazeAerodroma()
        {
            aerodrom1.Clear();
          
            using (SqlConnection conn = new SqlConnection())
            {
                 
                
                conn.ConnectionString = CONNECTION;
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM AERODROMI";
                SqlDataAdapter daAerodromi = new SqlDataAdapter();
                DataSet dsAerodromi = new DataSet();
                daAerodromi.SelectCommand = command;
                daAerodromi.Fill(dsAerodromi, "AerodromI");
                foreach (DataRow row in dsAerodromi.Tables["AerodromI"].Rows)
                {
                    Aerodrom aerodrom = new Aerodrom();
                   // aerodrom. = (int)row["id"];
                    aerodrom.Sifra = (string)row["Sifra"];
                    aerodrom.Naziv = (string)row["Naziv"];
                    aerodrom.NazivGrada = (string)row["Grad"];
                    aerodrom.Aktivan = (bool)row["Aktivan"];
                    aerodrom1.Add(aerodrom);
                }

            }
           
        }
        public void IspisIzKorisnika()
        {
            korisnici.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                
                conn.ConnectionString = CONNECTION;
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM korisnici";
                SqlDataAdapter daKorisnici = new SqlDataAdapter();
                DataSet dsKorisnici = new DataSet();
                daKorisnici.SelectCommand = command;
                daKorisnici.Fill(dsKorisnici, "Korisnici");
                foreach (DataRow row in dsKorisnici.Tables["Korisnici"].Rows)
                {
                    Korisnik aerodrom = new Korisnik();
                  
                    aerodrom.Ime = (string)row["Ime"];
                    aerodrom.Prezime = (string)row["Prezime"];
                    aerodrom.Adresa = (string)row["Adresa"];
                    aerodrom.Email = (string)row["Email"];
                    aerodrom.KorisnickoIme = (string)row["Korisnicko_Ime"];
                    aerodrom.Lozinka = (string)row["Lozinka"];
                    aerodrom.Pol = (string)row["Pol"];
                    aerodrom.TipKorisnka = (string)row["Tip_Korisnika"];
                    aerodrom.Aktivan = (bool)row["Aktivan"];
                    korisnici.Add(aerodrom);
                    
                }
                
            }
           

        }
        public void IspisIzAviokompanija()
        {
            aviokompanije.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                
                conn.ConnectionString = CONNECTION;
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM aviokmpanije";
                SqlDataAdapter daKorisnici = new SqlDataAdapter();
                DataSet dsKorisnici = new DataSet();
                daKorisnici.SelectCommand = command;
                daKorisnici.Fill(dsKorisnici, "Aviokmpanije");
                foreach (DataRow row in dsKorisnici.Tables["Aviokmpanije"].Rows)
                {
                    Aviokompanija aviokompanija = new Aviokompanija();
                    aviokompanija.Sifra = (int)row["Sifra"];
                    aviokompanija.Naziv = (string)row["Naziv"];
                    
                   
                    aviokompanija.Aktivan = (bool)row["Aktivan"];
                    aviokompanije.Add(aviokompanija);

                }

            }


        }

        public void IspisIzLetova()
        {
            let.Clear();
            using (SqlConnection conn = new SqlConnection())
            {

                conn.ConnectionString = CONNECTION;
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM letovi";
                SqlDataAdapter daKorisnici = new SqlDataAdapter();
                DataSet dsKorisnici = new DataSet();
                daKorisnici.SelectCommand = command;
                daKorisnici.Fill(dsKorisnici, "Letovi");
                foreach (DataRow row in dsKorisnici.Tables["Letovi"].Rows)
                {
                    Let aviokompanija = new Let();
                    aviokompanija.Sifra = (string)row["Sifra"];
                    aviokompanija.SifraAviokompanije = (int)row["SifraAviokompanije"];
                    aviokompanija.Odrediste = (string)row["Odrediste"];
                    aviokompanija.Destinacija = (string)row["Destinacija"];
                    aviokompanija.VremePolaska = (DateTime)row["VremePolaska"];
                    aviokompanija.VremeDolaska = (DateTime)row["VremeDolaska"];
                    aviokompanija.Cena = (int)row["Cena"];
                    


                    aviokompanija.Aktivan = (bool)row["Aktivan"];
                    aviokompanija.Lista = new List<Sedista>
                {
                    new Sedista{ Ime = "Red1M1B",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M2B",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M3B",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M4B",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M5B",
                                Zauzeto = false},
                   

                };
                    aviokompanija.Lista2 = new List<Sedista>
                {
                    new Sedista{ Ime = "Red1M1E",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M2E",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M3E",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M4E",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M5E",
                                Zauzeto = false},
                 

                };
                    let.Add(aviokompanija);

                }

            }


        }

        public void IspisIzKarata()
        {
            karte.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                karte.Clear();
                conn.ConnectionString = CONNECTION;
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM Karte";
                SqlDataAdapter daKorisnici = new SqlDataAdapter();
                DataSet dsKorisnici = new DataSet();
                daKorisnici.SelectCommand = command;
                daKorisnici.Fill(dsKorisnici, "Karte");
                foreach (DataRow row in dsKorisnici.Tables["Karte"].Rows)
                {
                    Karta aerodrom = new Karta();
                    
                    aerodrom.BrojLeta = (string)row["BrojLeta"];
                    aerodrom.BrojSedista = (string)row["BrojSedista"];
                    aerodrom.Putnik = (string)row["Putnik"];
                    aerodrom.Klasa = (string)row["Klasa"];
                    aerodrom.Cena = (int)row["Cena"];
                  
                    aerodrom.Aktivan = (bool)row["Aktivan"];
                    karte.Add(aerodrom);

                }
                conn.Close();
            }


        }
        private static Data _instance = null;

        public const String CONNECTION = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Platforme;Integrated Security=true";

        public static Data Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Data();
                }
                return _instance;
            }
        }         
        
        public void UcitajLetove()
        {

            let.Add(new Let
            {
                Sifra = "1234",
                SifraAviokompanije = 123,
                Destinacija = "BEG",
                Odrediste = "AMS",
                VremeDolaska = new DateTime(2018, 12, 1, 11, 00, 00),
                VremePolaska = new DateTime(2018, 12, 1, 9, 00, 00),
                Cena = 13000,
                Aktivan = true,
                Lista = new List<Sedista>
                {
                    new Sedista{ Ime = "Red1M1",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M2",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M3",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M4",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M5",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M6",
                                Zauzeto = false}

                }
            });
          

        }




       

    }
}

