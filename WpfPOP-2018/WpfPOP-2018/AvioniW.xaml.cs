﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for AvioniW.xaml
    /// </summary>
    public partial class AvioniW : Window
    {
        ICollectionView view;
        public AvioniW()
        {
            InitializeComponent();

            foreach (Korisnik korisnik in Data.Instance.korisnici)
            {
                if (korisnik.KorisnickoIme.Equals(Data.Instance.Ulogova) && korisnik.TipKorisnka.Equals("Putnik"))
                {
                    BtnDodaj.Visibility = Visibility.Hidden;
                    BtnIzmeni.Visibility = Visibility.Hidden;
                    BtnObrisi.Visibility = Visibility.Hidden;
                }
               
            }

                view = CollectionViewSource.GetDefaultView(Data.Instance.avioni);
            DGAvioni.ItemsSource = Data.Instance.avioni;

            view.Filter = CustomFilter;

            DGAvioni.IsSynchronizedWithCurrentItem = true;
            DGAvioni.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            DGAvioni.SelectedIndex = 0;



        }
       private bool CustomFilter(object obj)
        {
            Avion let = obj as Avion;
            if (txtSearch.Equals(String.Empty))
            {
                return let.Aktivan;
            }
            else
            {
                return let.Aktivan && let.Sifra.Contains(txtSearch.Text);
            }
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            IzmenaAvionaW edit = new IzmenaAvionaW(new Avion());
            edit.ShowDialog();
            DGAvioni.Items.Refresh();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
           Avion selektovan = (Avion)DGAvioni.SelectedItem;
            if (selektovan != null)
            {
                Avion stari = (Avion)selektovan.Clone();
                IzmenaAvionaW wLet = new IzmenaAvionaW(selektovan, IzmenaAvionaW.Opcija.izmena);
                if (wLet.ShowDialog() != true || validacija(selektovan)==true)
                {
                    int indeks = IndexLet(selektovan.Sifra);
                    Data.Instance.avioni[indeks] = stari;
                }

            }
            else
            {
                MessageBox.Show("Nije selektovan nijedan let");
                return;
            }
            selektovan.Izmena();
            DGAvioni.Items.Refresh();
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Avion let = (Avion)DGAvioni.SelectedItem;

            if (SelektovanLet(let))
            {
                if (MessageBox.Show("Da li ste sigurni", "Potvrda", MessageBoxButton.YesNoCancel).Equals(MessageBoxResult.Yes))
                {
                    var index = IndexLet(let.Sifra);
                    Data.Instance.avioni[index].Aktivan = false;
                    Avion selektovan = (Avion)DGAvioni.SelectedItem;
                    selektovan.Izmena();

                }
            }
            view.Refresh();


        }
        private int IndexLet(String let)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.avioni.Count; i++)
            {
                if (Data.Instance.avioni[i].Sifra.Equals(let))
                {
                    index = i;

                }

            }
            return index;

        }
        private bool SelektovanLet(Avion let)
        {
            if (let == null)
            {
                MessageBox.Show("Morate izabrati let");
                return false;
            }
            return true;
        }
       

       private bool validacija(Avion let)
        {
            if (let.Pilot.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti pilota");
                return false;
            }
            if (let.BrojLeta.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti pilota");
                return false;
            }
            if (let.NazivAviokompanije.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti naziv aviokompanije");
                return false;
            }

            return true;
        }



        private void BtnIme_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("Sifra", ListSortDirection.Ascending));
        }

        private void BtnPolazak_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("Destinacija", ListSortDirection.Ascending));
        }

        private void BtnDolazak_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("Cena", ListSortDirection.Descending));
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void dgFakulteti_AutoGeneratingColumn(object sender,
         DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Aktivan")
            {
                e.Cancel = true;
            }
        }
       
    }
}
