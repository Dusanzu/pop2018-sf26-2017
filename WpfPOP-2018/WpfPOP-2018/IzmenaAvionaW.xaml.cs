﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for IzmenaAvionaW.xaml
    /// </summary>
    public partial class IzmenaAvionaW : Window
    {
        public enum Opcija { Dodavanje, izmena };
        private Opcija opcija;
        private Avion avion;
        public IzmenaAvionaW(Avion avion, Opcija opcija = Opcija.Dodavanje)
        {
            this.avion = avion;
            this.opcija = opcija;
            this.DataContext = avion;
            InitializeComponent();
            cbAviokmpanija.ItemsSource= Data.Instance.aviokompanije.Select(a => a.Naziv);
            cbBrojLeta.ItemsSource = Data.Instance.let.Select(a => a.Sifra);

            if (opcija.Equals(Opcija.izmena))
            {
                txtSifra.IsReadOnly = true;
               
            }
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            if (opcija.Equals(Opcija.Dodavanje) || validacija() == true)
            {
                avion.Aktivan = true;


                avion.UnsUAviona();

            }
        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        public  bool validacija()
        {
            foreach (Avion aviokompanija in Data.Instance.avioni)
            {
                if (aviokompanija.Sifra.Equals(txtSifra.Text))
                {
                    MessageBox.Show("Sifra vec postoji");
                    return false;
                }
            }
            if (txtSifra.Text.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti sifru");
                return false;
            }
            if (txtPilot.Text.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti pilota");
                return false;
            }

            return true;
        }
    }
}
