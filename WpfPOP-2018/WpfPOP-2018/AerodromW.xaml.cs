﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for Aerodromi.xaml
    /// </summary>
    public partial class Aerodromi : Window
    {
         ICollectionView view;

        public Aerodromi()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.aerodrom1);
            view.Filter = CustomFilter;


                DGAerodromi.ItemsSource = Data.Instance.aerodrom1;
            DGAerodromi.IsSynchronizedWithCurrentItem = true;
            DGAerodromi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            foreach (Korisnik korisnik in Data.Instance.korisnici)
            {
                if (korisnik.KorisnickoIme.Equals(Data.Instance.Ulogova) && korisnik.TipKorisnka.Equals("Putnik"))
                {
                    BtnDodaj.Visibility = Visibility.Hidden;
                    BtnIzmeni.Visibility = Visibility.Hidden;
                    BtnObrisi.Visibility = Visibility.Hidden;
                }
              
            }
        


    }

          private bool CustomFilter(object obj)
          {
            Aerodrom aerodrom = obj as Aerodrom;
            if (txtSearch.Equals(String.Empty))
            {
                return aerodrom.Aktivan;
            }
            else
            {
                return aerodrom.Aktivan && aerodrom.Naziv.Contains(txtSearch.Text) || aerodrom.Sifra.Contains(txtSearch.Text) || aerodrom.NazivGrada.Contains(txtSearch.Text);
            }
        }

         

private void DGAerodromi_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            IzmenaAerodromaWindow edit = new IzmenaAerodromaWindow(new Aerodrom());
            edit.ShowDialog();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom selektovanAerodrom = (Aerodrom)DGAerodromi.SelectedItem;
            if (selektovanAerodrom != null)
            {
                Aerodrom stari = (Aerodrom)selektovanAerodrom.Clone();
                IzmenaAerodromaWindow wAerodrom = new IzmenaAerodromaWindow(selektovanAerodrom, IzmenaAerodromaWindow.Opcija.Izmena);
                if (wAerodrom.ShowDialog() != true || selektovanAerodrom.Naziv.Equals(string.Empty) || selektovanAerodrom.NazivGrada.Equals(string.Empty))
                {
                    int indeks = IndexA(selektovanAerodrom.Sifra);
                    Data.Instance.aerodrom1[indeks] = stari;
                }

            }
            else
            {
                MessageBox.Show("Nije selektovan ni jedan aerodrom");
                return;
            }
            selektovanAerodrom.Izmeni();
            DGAerodromi.Items.Refresh();

        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom aerodrom = (Aerodrom)DGAerodromi.SelectedItem;



            if (Selektovan(aerodrom))
            {
                if (MessageBox.Show("Da li ste sigurni", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var index = IndexA(aerodrom.Sifra);
                    Data.Instance.aerodrom1[index].Aktivan = false;
                    Aerodrom selektovanAerodrom = (Aerodrom)DGAerodromi.SelectedItem;
                    selektovanAerodrom.Izmeni();
                    //Data.Instance.aerodrom1.Remove(selektovanAerodrom);

                }
            }
            view.Refresh();

        }
        public bool Selektovan(Aerodrom aerodrom)
        {
            if (aerodrom == null)
            {
                MessageBox.Show("Morate izabrati aerodrom iz liste");
                return false;
            }
            return true;
        }
        public int IndexA(String aerodrom)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.aerodrom1.Count; i++)
            {
                if (Data.Instance.aerodrom1[i].Sifra.Equals(aerodrom))
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void BtnIme_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("Naziv", ListSortDirection.Ascending));

        }

        private void BtnGrad_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("NazivGrada", ListSortDirection.Ascending));
        }

        private void BtnSifra_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("Sifra", ListSortDirection.Ascending));
        }
       private void dgFakulteti_AutoGeneratingColumn(object sender,
           DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Aktivan")
            {
                e.Cancel = true;
            }
        }
    }
}
