﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for IzmenaAerodromaWindow.xaml
    /// </summary>
    public partial class IzmenaAerodromaWindow : Window
    {
        public enum Opcija { Dodavanje, Izmena }
        private Opcija opcija;
        private Aerodrom aerodrom;
        public IzmenaAerodromaWindow(Aerodrom aerodrom, Opcija opcija = Opcija.Dodavanje)
        {
            InitializeComponent();
            this.aerodrom = aerodrom;
            this.opcija = opcija;

            this.DataContext = aerodrom;

        }
       

        private void Odustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            if (opcija.Equals(Opcija.Dodavanje) && validacija()==true)
            {
                aerodrom.Aktivan = true;
                aerodrom.UnsUAerodrome();
            }
        }
        public Boolean validacija()
        {
            if (TxtSifra.Text.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti sifru aerodroma");
                //Sacuvaj.Visibility = Visibility.Hidden;
                return false;
            }
            foreach(Aerodrom aerodrom in Data.Instance.aerodrom1)
            {
                
                
                    if (TxtSifra.Text.Equals(aerodrom.Sifra))
                    {
                        MessageBox.Show("Sifru postoji");
                       // Sacuvaj.Visibility = Visibility.Hidden;
                        return false;
                    }
                
               
            }
            if (TxtNaziv.Text.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti naziv aerodroma");
               // Sacuvaj.Visibility = Visibility.Hidden;
                return false;
            }
            if (TxtGrad.Text.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti naziv grada");
               // Sacuvaj.Visibility = Visibility.Hidden;
                return false;
            }

            return true;
        }
       
    }

    }

