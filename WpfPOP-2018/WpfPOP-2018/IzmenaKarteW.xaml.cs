﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for IzmenaKarteW.xaml
    /// </summary>
    public partial class IzmenaKarteW : Window

    {

        public enum Opcija { Dodavanje, Izmena }
        private Opcija opcija;
        private Karta karta;
        public IzmenaKarteW(Karta karta, Opcija opcija = Opcija.Dodavanje)
        {
            InitializeComponent();

            this.karta = karta;
            this.opcija = opcija;

            this.DataContext = karta;

            cbBrojLeta.ItemsSource = Data.Instance.let.Select(a => a.Sifra);
         //   cbBrojSedista.ItemsSource = Data.Instance.sedista.Select(a => a.SlobodnaSedista);
            cbKlasa.ItemsSource = Data.Instance.karte.Select(a => a.Klasa);

            foreach(Let let in Data.Instance.let)
            {
                if (let.Sifra.Equals(cbBrojLeta.SelectedItem))
                {
                    if (cbKlasa.SelectedItem.Equals("Ekonomska"))
                    {
                        karta.Cena = (int)let.Cena;
                    }
                    else if (cbKlasa.SelectedItem.Equals("Biznis"))
                    {
                        karta.Cena = (int)let.Cena+ (int)let.Cena/2;
                    }
                }
            }
            
        }

        private void BtnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

       private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            if (opcija.Equals(Opcija.Izmena) && validacija()==true)
            {
                karta.Aktivan = true;
                karta.Izmena();
            }
        }
        public bool validacija()
        { 
            if (cbBrojLeta.SelectedItem == null)
            {
                MessageBox.Show("Morate uneti broj leta");
                return false;
            }
            if (cbKlasa.SelectedItem == null)
            {
                MessageBox.Show("Morate uneti klasu");
                return false;
            }
          
            return true;
        }
    }
}
