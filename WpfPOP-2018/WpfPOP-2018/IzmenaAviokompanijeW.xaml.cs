﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for IzmenaAviokompanijeW.xaml
    /// </summary>
    public partial class IzmenaAviokompanijeW : Window
    {
        public enum Opcija { Dodavanje,Izmena};
        private Opcija opcija;
       public Aviokompanija aviokompanija;
        public ObservableCollection<Let> letovi { get; set; }
        public IzmenaAviokompanijeW(Aviokompanija aviokompanija, Opcija opcija = Opcija.Dodavanje)
        {


            this.aviokompanija = aviokompanija;
            this.opcija = opcija;
            this.DataContext = aviokompanija;
            InitializeComponent();
            letovi = new ObservableCollection<Let>();

            foreach(Let let in Data.Instance.let)
            {
                if (let.SifraAviokompanije.Equals(aviokompanija.Sifra))
                    letovi.Add(let);
            }

            dgLet.ItemsSource = letovi;

            if (opcija.Equals(Opcija.Izmena))
            {
                txtSifra.IsReadOnly = true;
            }

        }

       
    

        

        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            if (opcija.Equals(Opcija.Dodavanje) && validacija()==true)
            {
                aviokompanija.Aktivan = true;
                aviokompanija.UnsUAviokompanije();

            }
        }

        private void Odustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

       

        public bool validacija()
        {
            if (Convert.ToInt32(txtSifra.Text) <= 0)
            {
                MessageBox.Show("Cena mora biti veca od 0");
                return false;
            }
            try
            {
                Convert.ToInt32(txtSifra.Text);
            }
            catch (Exception e)
            {
                MessageBox.Show("Sifra mora biti broj" + e.Message);
                return false;

            }
            foreach (Aviokompanija aviokompanija in Data.Instance.aviokompanije)
            {
                if (aviokompanija.Sifra.Equals(txtSifra.Text))
                {
                    MessageBox.Show("Sifra vec postoji");
                    return false;
                }
            }
            if (txtSifra.Text.Equals(""))
            {
                MessageBox.Show("Nije uneta sifra");
                return false;
            }
            if (txtName.Text.Equals(string.Empty))
            {
                MessageBox.Show("Nije unet naziv");
                return false;
            }

            return true;
        }
    }
}
