﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfPOP_2018.validation
{
    class EmailValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
           if(value.ToString().Contains("@") && value.ToString().EndsWith(".com"))
            {
                return new ValidationResult(true, "corect");
            }
            return new ValidationResult(false, "Incorect");
        }
    }
}
