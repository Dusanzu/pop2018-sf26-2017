﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfPOP_2018.Database;

namespace WpfPOP_2018.Model
{
    public class Aerodrom : INotifyPropertyChanged, ICloneable
    {
        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; Changed("Sifra"); }
        }

        private string naziv;

        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; Changed("Naziv"); }
        }
        private string nazivGrada;

        public string NazivGrada
        {
            get { return nazivGrada; }
            set { nazivGrada = value; Changed("NazivGrada"); }
        }
        private Boolean aktivan;

        public Boolean Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; Changed("Aktivan"); }
        }





        // public object OnePropertyChanged { get; private set; }





        /*  public String sifra { get; set; }
          public String naziv { get; set; }
          public String nazivGrada { get; set; }
          public bool Aktivan { get; set; }*/

        public Aerodrom() { }

        public Aerodrom(String sifra, String naziv, String nazivGrada)
        {
            this.sifra = sifra;
            this.naziv = naziv;
            this.nazivGrada = nazivGrada;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return $"Sifra Aerodroma{sifra},Naziv Aerodroma{naziv},Naziv grada{nazivGrada}";

        }
        private void Changed(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }

        }
    
        public object Clone() {

            Aerodrom aerodrom = new Aerodrom
            {
                Sifra = this.sifra,
                Aktivan = this.aktivan,
                Naziv = this.naziv,
                nazivGrada = this.nazivGrada


            };
            return aerodrom;


        }
        public void UnsUAerodrome()
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = Data.CONNECTION;

                con.Open();

                SqlCommand command = con.CreateCommand();
                command.CommandText = @"insert into aerodromi values(@Sifra,@Naziv, @Grad, @Aktivan)";


                
                command.Parameters.Add(new SqlParameter("@Naziv", this.Naziv));
                command.Parameters.Add(new SqlParameter("@Grad", this.NazivGrada));
                command.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));
                command.ExecuteNonQuery();

               // con.Close();

            }
            Data.Instance.IspisIzUBazeAerodroma();
          
        }

        public void Izmeni()
        {
            SqlConnection con = new SqlConnection();
           try
            {
                con.ConnectionString = Data.CONNECTION;

                con.Open();

                SqlCommand command = con.CreateCommand();
                command.CommandText = @"update  Aerodromi set Sifra=@Sifra, Naziv=@Naziv, Grad=@Grad , Aktivan=@Aktivan";



                command.Parameters.Add(new SqlParameter("@Naziv", this.Naziv));
                command.Parameters.Add(new SqlParameter("@Grad", this.NazivGrada));
                command.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));
                command.ExecuteNonQuery();

                // con.Close();

            }
            catch (Exception ex)
            {
                Console.Write($"Exception{ex}");
            }
            finally { con.Close();
            }
        }


    }

   
}

