﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfPOP_2018.Database;

namespace WpfPOP_2018.Model
{
    public enum Pol { Muski,Zenski}
    public enum TipKorisnika { Admin,Korisnik}
    public class Korisnik : INotifyPropertyChanged,ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string ime;

        public  string Ime
        {
            get { return ime; }
            set { ime = value; Changed("Ime"); }
        }
        private string prezime;

        public string Prezime
        {
            get { return prezime; }
            set { prezime = value; Changed("Prezime"); }
        }
        private string email;

        public  string Email
        {
            get { return email; }
            set { email = value; Changed("Email"); }
        }
        private string adresa;

        public  string Adresa
        {
            get { return adresa; }
            set { adresa = value; Changed("Adresa"); }
        }
        private string korisnickoIme;

        public string KorisnickoIme
        {
            get { return korisnickoIme; }
            set { korisnickoIme = value; Changed("KorisnickoIme"); }
        }
        private string lozinka;

        public string Lozinka
        {
            get { return lozinka;; }
            set { lozinka = value; Changed("Loinka"); }
        }
        private string pol;

        public string Pol
        {
            get { return pol; }
            set { pol = value; Changed("Pol"); }
        }
        private string tipKorisnika;

        public string TipKorisnka
        {
            get { return tipKorisnika; }
            set { tipKorisnika = value; Changed("TipKorisnika"); }
        }
        private Boolean aktivan;

        public Boolean Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; Changed("Aktivan"); }
        }

        public void Changed(string name)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }

        }
        public void UnosUBazu()
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = Data.CONNECTION;

                con.Open();

                SqlCommand command = con.CreateCommand();
                command.CommandText = @"insert into Korisnici(Ime,Prezime,Adresa,Email,Korisnicko_Ime,Lozinka,Pol,Tip_Korisnika,Aktivan) values(@Ime , @Prezime, @Adresa , @Email , @Korisnicko_Ime , @Lozinka , @Pol , @Tip_Korisnika , @Aktivan)";



                command.Parameters.Add(new SqlParameter("@Ime", this.Ime ));
                command.Parameters.Add(new SqlParameter("@Prezime", this.Prezime));
                command.Parameters.Add(new SqlParameter("@Adresa", this.Adresa));
                command.Parameters.Add(new SqlParameter("@Email", this.Email));
                command.Parameters.Add(new SqlParameter("@Korisnicko_Ime", this.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@lozinka", this.Lozinka));
                command.Parameters.Add(new SqlParameter("@Pol", this.Pol));
                command.Parameters.Add(new SqlParameter("@Tip_Korisnika", this.TipKorisnka));
                command.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));
                command.ExecuteNonQuery();

                // con.Close();

            }
            Data.Instance.IspisIzKorisnika();

        }
        public void Izmeni()
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = Data.CONNECTION;

                con.Open();

                SqlCommand command = con.CreateCommand();
                command.CommandText = @"update Korisnici set Ime=@Ime , Prezime=@Prezime, Adresa=@Adresa,Email=@Email , Korisnicko_Ime=@Korisnicko_Ime,Lozinka=@Lozinka , Pol=@Pol,Tip_Korisnika=@Tip_Korisnika,Aktivan= @Aktivan";



                command.Parameters.Add(new SqlParameter("@Ime", this.Ime));
                command.Parameters.Add(new SqlParameter("@Prezime", this.Prezime));
                command.Parameters.Add(new SqlParameter("@Adresa", this.Adresa));
                command.Parameters.Add(new SqlParameter("@Email", this.Email));
                command.Parameters.Add(new SqlParameter("@Korisnicko_Ime", this.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@lozinka", this.Lozinka));
                command.Parameters.Add(new SqlParameter("@Pol", this.Pol));
                command.Parameters.Add(new SqlParameter("@Tip_Korisnika", this.TipKorisnka));
                command.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));
                command.ExecuteNonQuery();

               

            }
            
        }







        public object Clone()
        {
            var korisnik = new Korisnik
            {
                Ime = this.ime,
                Prezime = this.prezime,
                Email = this.email,
                Adresa = this.adresa,
                Lozinka = this.lozinka,
                Pol = this.pol,
                TipKorisnka=this.tipKorisnika,
                KorisnickoIme=this.korisnickoIme,
                Aktivan=this.aktivan

            };
            return korisnik;
        }
    }
}
