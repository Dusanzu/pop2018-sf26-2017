﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfPOP_2018.Database;

namespace WpfPOP_2018.Model
{
    public class Karta : INotifyPropertyChanged , ICloneable
    {

        


        private string brojLeta;

        public string BrojLeta
        {
            get { return brojLeta; }
            set { brojLeta = value; Changed("BrojLeta"); }
        }



        private string brojSedista;

        public string BrojSedista
        {
            get { return brojSedista; }
            set { brojSedista = value; }
        }

        private string putnik;

        public string Putnik
        {
            get { return putnik; }
            set { putnik = value; Changed("Putnik"); }
        }
        private string klasa;

        public string Klasa
        {
            get { return klasa; }
            set { klasa = value; Changed("Klasa"); }
        }
        private int cena;

        public int Cena
        {
            get { return cena; }
            set { cena = value; Changed("Cena"); }
        }
        private Boolean aktivan;
        public Boolean Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; Changed("Aktivan"); }
        }
        public void UnsUAerodrome()
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = Data.CONNECTION;

                con.Open();

                SqlCommand command = con.CreateCommand();
                command.CommandText = @"insert into Karte values(@BrojLeta,@BrojSedista, @Putnik,@Klasa, @Cena, @Aktivan)";


               
                command.Parameters.Add(new SqlParameter("@BrojLeta", this.BrojLeta));
                command.Parameters.Add(new SqlParameter("@BrojSedista", this.BrojSedista));
                command.Parameters.Add(new SqlParameter("@Putnik", this.Putnik));
                command.Parameters.Add(new SqlParameter("@Klasa", this.Klasa));
                command.Parameters.Add(new SqlParameter("@Cena", this.Cena));
                command.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));
                command.ExecuteNonQuery();

                // con.Close();

            }
            Data.Instance.IspisIzKarata();

        }

        public void Izmena()
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = Data.CONNECTION;

                con.Open();

                SqlCommand command = con.CreateCommand();
                command.CommandText = @"update Karte set  BrojLeta=@BrojLeta,BrojSedista=@BrojSedista ,Putnik=@Putnik,Klasa=@Klasa, Cena=@Cena, Aktivan=@Aktivan";



              
                command.Parameters.Add(new SqlParameter("@BrojLeta", this.BrojLeta));
                command.Parameters.Add(new SqlParameter("@BrojSedista", this.BrojSedista));
                command.Parameters.Add(new SqlParameter("@Putnik", this.Putnik));
                command.Parameters.Add(new SqlParameter("@Klasa", this.Klasa));
                command.Parameters.Add(new SqlParameter("@Cena", this.Cena));
                command.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));
                command.ExecuteNonQuery();

                // con.Close();

            }
            

        }













        public event PropertyChangedEventHandler PropertyChanged;

        private void Changed(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }

        }

        public object Clone()
        {
            Karta karta = new Karta
            {
                
                BrojLeta = this.brojLeta,
                BrojSedista = this.brojSedista,
                Putnik = this.putnik,
                Klasa = this.klasa,
                Cena = this.cena,
                Aktivan = this.aktivan


            };
            return karta;

        }

    }
}
