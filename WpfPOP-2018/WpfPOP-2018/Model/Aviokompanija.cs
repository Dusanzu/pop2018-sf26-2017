﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfPOP_2018.Database;

namespace WpfPOP_2018.Model
{
   public class Aviokompanija : INotifyPropertyChanged, ICloneable
    {

        private int sifra;

        public int Sifra
        {
            get { return sifra; }
            set { sifra = value;Changed("Sifra"); }
        }


        private string naziv;

        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; Changed("Naziv"); }
        }

      
        private Boolean aktivan;

        public Boolean Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; Changed("Aktivan"); }
        }
        private void Changed(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }

        }






        public event PropertyChangedEventHandler PropertyChanged;

        public object Clone()
        {
            Aviokompanija aviokompanija = new Aviokompanija
            {
                Sifra = this.sifra,
                Naziv = this.naziv,
                
                Aktivan = this.aktivan


            };
            return aviokompanija;


        }
        public void UnsUAviokompanije()
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = Data.CONNECTION;

                con.Open();

                SqlCommand command = con.CreateCommand();
                command.CommandText = @"insert into aviokmpanije values(@Sifra,@Naziv, @Aktivan)";



                command.Parameters.Add(new SqlParameter("@Naziv", this.Naziv));
                
                command.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));
                command.ExecuteNonQuery();

                // con.Close();

            }
            Data.Instance.IspisIzAviokompanija();

        }
        public void Izmeni()
        {
            SqlConnection con = new SqlConnection();
            try
            {
                con.ConnectionString = Data.CONNECTION;

                con.Open();

                SqlCommand command = con.CreateCommand();
                command.CommandText = @"update  Aviokmpanije set Sifra=@Sifra, Naziv=@Naziv , Aktivan=@Aktivan";



                command.Parameters.Add(new SqlParameter("@Naziv", this.Naziv));
          
                command.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));
                command.ExecuteNonQuery();

                // con.Close();

            }
            catch (Exception ex)
            {
                Console.Write($"Exception{ex}");
            }
            finally
            {
                con.Close();
            }
        }


    }
}

