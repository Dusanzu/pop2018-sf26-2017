﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfPOP_2018.Database;

namespace WpfPOP_2018.Model
{
    
    public class Avion : INotifyPropertyChanged, ICloneable
    {

     

        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; Changed("Sifra"); }
        }
        private string pilot;

        public string Pilot
        {
            get { return pilot; }
            set { pilot = value; Changed("Pilot"); }
        }

        private string brojLeta;

        public string BrojLeta
        {
            get { return brojLeta; }
            set { brojLeta = value; Changed("BrojLeta"); }
        }
       

       
        private string nazivAviokompanije;

        public string NazivAviokompanije
        {
            get { return nazivAviokompanije; }
            set { nazivAviokompanije = value; Changed("NazivAviokompanije"); }
        }

        private Boolean aktivan;

        public Boolean Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; Changed("Aktivan"); }
        }


        private void Changed(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }

        }





        public event PropertyChangedEventHandler PropertyChanged;

        public object Clone()
        {
            Avion avion = new Avion
            {

                Sifra = this.sifra,
                Pilot = this.pilot,
                BrojLeta = this.brojLeta,
                NazivAviokompanije = this.nazivAviokompanije,
                Aktivan = this.aktivan

            };
            return avion;
        }
        public void UnsUAviona()
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = Data.CONNECTION;

                con.Open();

                SqlCommand command = con.CreateCommand();
                command.CommandText = @"insert into avioni values(@Sifra,@Pilot, @BrojLeta,@NazivAviokompanije, @Aktivan)";



               
                command.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@Pilot", this.Pilot));
                command.Parameters.Add(new SqlParameter("@BrojLeta", this.BrojLeta));
                command.Parameters.Add(new SqlParameter("@NazivAviokompanije", this.NazivAviokompanije));
                command.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));
                command.ExecuteNonQuery();

                // con.Close();

            }
            Data.Instance.IspisIzUBazeAviona();

        }

        public void Izmena()
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = Data.CONNECTION;

                con.Open();

                SqlCommand command = con.CreateCommand();
                command.CommandText = @"update  avioni set Sifra=@Sifra,Pilot=@Pilot, BrojLeta=@BrojLeta,NazivAviokompanije=@NazivAviokompanije,Aktivan= @Aktivan";




                command.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@Pilot", this.Pilot));
                command.Parameters.Add(new SqlParameter("@BrojLeta", this.BrojLeta));
                command.Parameters.Add(new SqlParameter("@NazivAviokompanije", this.NazivAviokompanije));
                command.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));
                command.ExecuteNonQuery();

                // con.Close();

            }
         

        }
    }
}
