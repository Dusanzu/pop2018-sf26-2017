﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfPOP_2018.Database;

namespace WpfPOP_2018.Model
{
    
   public class Let : INotifyPropertyChanged, ICloneable
    {

        private List<Sedista> lista;
       

        public List<Sedista> Lista
        {
            get { return lista; }
            set { lista = value; }
        }
        private List<Sedista> lista2;


        public List<Sedista> Lista2
        {
            get { return lista2; }
            set { lista2 = value; }
        }



        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; Changed("Sifra"); }
        }
        private int sifraAviokompanije;

        public int SifraAviokompanije
        {
            get { return sifraAviokompanije; }
            set { sifraAviokompanije = value;Changed("SifraAviokompanije"); }
        }

        private string destinacija;

        public string Destinacija
        {
            get { return destinacija; }
            set { destinacija = value; Changed("Destinacija"); }
        }
        private string odrediste;

        public string Odrediste
        {
            get { return odrediste; }
            set { odrediste = value; Changed("Odrediste"); }
        }

        private DateTime vremePolaska;

        public event PropertyChangedEventHandler PropertyChanged;

        public DateTime VremePolaska
        {
            get { return vremePolaska; }
            set { vremePolaska = value; Changed("VremePolaska"); }
        }
        private DateTime vremeDolaska;

        public DateTime VremeDolaska
        {
            get { return vremeDolaska; }
            set { vremeDolaska = value; Changed("VremeDolaska"); }
        }
        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; Changed("Aktivan"); }
        }
        private double cena;

        

        public double Cena
        {
            get { return cena; }
            set { cena = value;Changed("Cena"); }
        }

        








        private void Changed(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }

        }
        public object Clone()
        {

            Let let = new Let
            {
                Sifra = this.sifra,
                SifraAviokompanije=this.sifraAviokompanije,
                Destinacija = this.destinacija,
                Odrediste = this.odrediste,
                VremePolaska = this.vremePolaska,
                VremeDolaska = this.vremePolaska,
                Cena = this.cena,
                Lista=this.lista,
                Lista2 = this.lista2,
                Aktivan = this.aktivan


            };
            return let;


        }
        public void UnsULetove()
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = Data.CONNECTION;

                con.Open();

                SqlCommand command = con.CreateCommand();
                command.CommandText = @"insert into letovi values(@Sifra,@SifraAviokompanije,@Destinacija,@Odrediste, @VremePolaska,@VremeDolaska,@Cena @Aktivan)";



                command.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@SifraAviokompanije", this.SifraAviokompanije));
                command.Parameters.Add(new SqlParameter("@Destinacija", this.Destinacija));
                command.Parameters.Add(new SqlParameter("@Odrediste", this.Odrediste));
                command.Parameters.Add(new SqlParameter("@VremePolaska", this.VremePolaska));
                command.Parameters.Add(new SqlParameter("@VremeDolaska", this.VremeDolaska));
                command.Parameters.Add(new SqlParameter("@Cena", this.Cena));
                command.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));
                command.ExecuteNonQuery();

                // con.Close();

            }
            Data.Instance.IspisIzKarata();

        }
        public void izmena()
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = Data.CONNECTION;

                con.Open();

                SqlCommand command = con.CreateCommand();
                command.CommandText = @"update letovi set Sifra=@Sifra, SifraAviokompanije=@SifraAviokompanije, Destinacija=@Destinacija,Odrediste=@Odrediste,VremePolaska= @VremePolaska, VremeDolaska=@VremeDolaska,Cena=@Cena, Aktivan= @Aktivan";



                command.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@SifraAviokompanije", this.SifraAviokompanije));
                command.Parameters.Add(new SqlParameter("@Destinacija", this.Destinacija));
                command.Parameters.Add(new SqlParameter("@Odrediste", this.Odrediste));
                command.Parameters.Add(new SqlParameter("@VremePolaska", this.vremePolaska));
                command.Parameters.Add(new SqlParameter("@VremeDolaska", this.vremeDolaska));
                command.Parameters.Add(new SqlParameter("@Cena", this.Cena));
                command.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));
                command.ExecuteNonQuery();

                // con.Close();

            }
           

        }
    }
}

