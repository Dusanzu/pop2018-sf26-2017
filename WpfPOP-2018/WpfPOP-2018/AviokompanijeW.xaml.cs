﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for AviokompanijeW.xaml
    /// </summary>
    public partial class AviokompanijeW : Window
    {
        ICollectionView view;
        public AviokompanijeW()
        {
            InitializeComponent();

            view = CollectionViewSource.GetDefaultView(Data.Instance.aviokompanije);
             view.Filter = CustomFilter;

            DGAviokompanije.ItemsSource = Data.Instance.aviokompanije;
            DGAviokompanije.IsSynchronizedWithCurrentItem = true;
            DGAviokompanije.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

            foreach (Korisnik korisnik in Data.Instance.korisnici)
            {
                if (korisnik.KorisnickoIme.Equals(Data.Instance.Ulogova) && korisnik.TipKorisnka.Equals("Putnik"))
                {
                    BtnDodaj.Visibility = Visibility.Hidden;
                    BtnIzmeni.Visibility = Visibility.Hidden;
                    BtnObrisi.Visibility = Visibility.Hidden;
                }
                
            }
        }
        
        private bool CustomFilter(object obj)
        {
            Aviokompanija aerodrom = obj as Aviokompanija;
            if (txtSearch.Equals(String.Empty))
            {
                return aerodrom.Aktivan;
            }
            else
            {
                return aerodrom.Aktivan && aerodrom.Naziv.Contains(txtSearch.Text) ;
            }
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            IzmenaAviokompanijeW av = new IzmenaAviokompanijeW(new Aviokompanija());
            av.ShowDialog();

        }
        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Aviokompanija selektovanLet = (Aviokompanija)DGAviokompanije.SelectedItem;
            if (selektovanLet != null)
            {
                Aviokompanija stari = (Aviokompanija)selektovanLet.Clone();
                IzmenaAviokompanijeW wLet = new IzmenaAviokompanijeW(selektovanLet, IzmenaAviokompanijeW.Opcija.Izmena);
                if (wLet.ShowDialog() != true || selektovanLet.Naziv.Equals(string.Empty))
                {
                    int indeks = IndexA(selektovanLet.Sifra);
                    Data.Instance.aviokompanije[indeks] = stari;
                }

            }
            else
            {
                MessageBox.Show("Nije selektovan nijedna aviokompanija");
                return;
            }
            selektovanLet.Izmeni();
            DGAviokompanije.Items.Refresh();


        }
       

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Aviokompanija avion = (Aviokompanija)DGAviokompanije.SelectedItem;



            if (Selektovan(avion))
            {
                if (MessageBox.Show("Da li ste sigurni", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var index = IndexA(avion.Sifra);
                    Data.Instance.aviokompanije[index].Aktivan = false;
                    Aviokompanija selektovanAerodrom = (Aviokompanija)DGAviokompanije.SelectedItem;
                    selektovanAerodrom.Izmeni();
                   

                }
            }
            view.Refresh();
        }

        private void DGAvioni_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        public bool Selektovan(Aviokompanija avion)
        {
            if (avion == null)
            {
                MessageBox.Show("Morate izabrati aviokompaniju iz liste");
                return false;
            }
            return true;
        }
        public int IndexA(int avion)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.aviokompanije.Count; i++)
            {
                if (Data.Instance.aviokompanije[i].Sifra.Equals(avion))
                {
                    index = i;
                    break;
                }
            }
            return index;
        }


        private void DGAviokompanije_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        private void dgFakulteti_AutoGeneratingColumn(object sender,
         DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Aktivan" /*|| (string)e.Column.Header == "Aktivan"*/)
            {
                e.Cancel = true;
            }
        }

    }
}
