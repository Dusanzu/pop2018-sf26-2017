﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for IzmenaLetaW.xaml
    /// </summary>
    public partial class IzmenaLetaW : Window
    {
        public enum Opcija { Dodavanje, izmena };
        private Opcija opcija;
        public Let let { get; set; }
       // private Aviokompanija aviokompanija;
        public IzmenaLetaW(Let let,  Opcija opcija = Opcija.Dodavanje)
        {
            InitializeComponent();
            
            let = new Let();
            this.opcija = opcija;
            this.DataContext = let;
            InitializeComponent();

            cbOdrediste.ItemsSource = Data.Instance.aerodrom1.Select(a => a.Sifra);
            cbPolaziste.ItemsSource = Data.Instance.aerodrom1.Select(a => a.Sifra);
            if (opcija.Equals(Opcija.izmena))
            {
                TxtSifra.IsReadOnly = true;
            }
        }
        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = true;
            this.Close();
        }

        private void Odustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

        }
        public bool Postoji(string sifra)
        {
            foreach (Let let in Data.Instance.let)
            {
                
                    if (let.Sifra.Equals(sifra))
                    {
                        return true;
                    }
                
            }
            return false;
        }
    }
}
