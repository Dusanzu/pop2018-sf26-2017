﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for KorisniciW.xaml
    /// </summary>
    public partial class KorisniciW : Window
    { 
        private ICollectionView view;
    
        public KorisniciW()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.korisnici);
            view.Filter = CustomFilter;

            DGKorisnici.ItemsSource = Data.Instance.korisnici;
            DGKorisnici.IsSynchronizedWithCurrentItem = true;
            DGKorisnici.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

            foreach (Korisnik korisnik in Data.Instance.korisnici)
            {
                if (korisnik.KorisnickoIme.Equals(Data.Instance.Ulogova))
                {
                    btnDodaj.Visibility = Visibility.Hidden;
                    btnIzmeni.Visibility = Visibility.Hidden;
                    btnIzbrisi.Visibility = Visibility.Hidden;
                }
                else
                {
                    btnDodaj.Visibility = Visibility.Visible;
                    btnIzmeni.Visibility = Visibility.Visible;
                    btnIzbrisi.Visibility = Visibility.Visible;
                }
            }
        

    }
        

    private bool CustomFilter(object obj)
        {
            Korisnik korisnik = obj as Korisnik;
            if (txtSearch.Equals(String.Empty))
            {
                return korisnik.Aktivan;
            }
            else
            {
                return korisnik.Aktivan && korisnik.KorisnickoIme.Contains(txtSearch.Text) || korisnik.Ime.Contains(txtSearch.Text) || korisnik.Prezime.Contains(txtSearch.Text) || korisnik.Adresa.Contains(txtSearch.Text);
            }
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            IzmenaKorisnikaW wAerodrom = new IzmenaKorisnikaW(new Korisnik());
            wAerodrom.ShowDialog();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Korisnik selektovanaKorisnik = (Korisnik)DGKorisnici.SelectedItem;
            if (selektovanaKorisnik != null)
            {
                Korisnik stari = (Korisnik)selektovanaKorisnik.Clone();
                IzmenaKorisnikaW wAerodrom = new IzmenaKorisnikaW(selektovanaKorisnik, IzmenaKorisnikaW.Opcija.Izmena);
                if (wAerodrom.ShowDialog() != true || selektovanaKorisnik.Ime.Equals(string.Empty) || selektovanaKorisnik.Prezime.Equals(string.Empty) || selektovanaKorisnik.Adresa.Equals(string.Empty))
                {
                    int indeks = IndexKarte(selektovanaKorisnik.KorisnickoIme);
                    Data.Instance.korisnici[indeks] = stari;
                }

            }
            else
            {
                MessageBox.Show("Nije selektovan ni jedan aerodrom");
                return;
            }
            selektovanaKorisnik.Izmeni();
            DGKorisnici.Items.Refresh();

        }

        private void BtnIzbrisi_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = (Korisnik)DGKorisnici.SelectedItem;



            if (Selektovan(korisnik))
            {
                if (MessageBox.Show("Da li ste sigurni", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var index = IndexKarte(korisnik.KorisnickoIme);
                    Data.Instance.korisnici[index].Aktivan = false;
                    Korisnik selektovanAerodrom = (Korisnik)DGKorisnici.SelectedItem;
                    selektovanAerodrom.Izmeni();

                }
            }
            view.Refresh();

        }
        

        private void BtnIme_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("Ime", ListSortDirection.Ascending));
        }

        private void BtnPrezime_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("Prezime", ListSortDirection.Ascending));
        }

        private void BtnKorisnicko_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("KorisnickoIme", ListSortDirection.Ascending));
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        public int IndexKarte(string sifra)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.korisnici.Count; i++)
            {
                if (Data.Instance.korisnici[i].KorisnickoIme.Equals(sifra))
                {

                    index = i;
                    break;
                }
            }
            return index;

        }
        public bool Selektovan(Korisnik korisnik)
        {
            if (korisnik == null)
            {
                MessageBox.Show("Morate izabrati korisnika iz liste");
                return false;
            }
            return true;
        }

        private void DGKorisnici_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        private void dgFakulteti_AutoGeneratingColumn(object sender,
           DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Aktivan")
            {
                e.Cancel = true;
            }
        }
    }
}
