﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for NalogW.xaml
    /// </summary>
    public partial class NalogW : Window
    {
        Korisnik korisnik;
        public NalogW(Korisnik korisnik)
        {
            this.korisnik = korisnik;
            InitializeComponent();

            lbKorisnik.Content = "Logovani korisnik:"+korisnik.KorisnickoIme;
            Data.Instance.Ulogova = korisnik.KorisnickoIme;

           if (korisnik.TipKorisnka.Equals("Admin"))
            {

                btnNalog.Visibility = Visibility.Hidden;
                gj.Visibility = Visibility.Hidden;

            }
            else
            {
                btnKorisnici.Visibility = Visibility.Hidden;
                btnKarte.Visibility = Visibility.Hidden;
            }
        }

        private void BtnNalog_Click(object sender, RoutedEventArgs e)
        {
            IzmenaKorisnikaW i = new IzmenaKorisnikaW(korisnik, IzmenaKorisnikaW.Opcija.Izmena);
            i.ShowDialog();
        }

        private void BtnKorisnici_Click(object sender, RoutedEventArgs e)
        {
            KorisniciW k = new KorisniciW();
            k.ShowDialog();

        }

        private void BtnLet_Click(object sender, RoutedEventArgs e)
        {
            Letovi l = new Letovi();
            l.ShowDialog();
        }

        private void BtnAvioni_Click(object sender, RoutedEventArgs e)
        {
            AviokompanijeW d = new AviokompanijeW();
            d.ShowDialog();
        }

        private void BtnAerodromi_Click(object sender, RoutedEventArgs e)
        {
            Aerodromi s = new Aerodromi();
            s.ShowDialog();
        }

        private void BtnKarte_Click(object sender, RoutedEventArgs e)
        {
            KarteW g = new KarteW();
            g.ShowDialog();
        }

        private void Gj_Click(object sender, RoutedEventArgs e)
        {
            KarteKorisnikaW j = new KarteKorisnikaW();
            j.Show();
        }

        private void BtnAvion_Click(object sender, RoutedEventArgs e)
        {
            AvioniW d = new AvioniW();
            d.ShowDialog();
        }
    }
}
