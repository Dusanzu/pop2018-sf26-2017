﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for EditLet.xaml
    /// </summary>
    public partial class EditLet : Window
    {
        public enum Opcija { Dodavanje, izmena };
        private Opcija opcija;
        private Let let;
        private Aviokompanija aviokompanija;
        public EditLet(Let let,Aviokompanija aviokompanija, Opcija opcija = Opcija.Dodavanje)
        {
            this.let = let;
            this.opcija = opcija;
            this.DataContext = let;
            this.aviokompanija = aviokompanija;
            InitializeComponent();

            cbOdrediste.ItemsSource = Data.Instance.aerodrom1.Select(a => a.Sifra);
            cbPolaziste.ItemsSource = Data.Instance.aerodrom1.Select(a => a.Sifra);
            cbAviokompanija.ItemsSource = Data.Instance.aviokompanije.Select(a => a.Sifra);

            // dpPolazak = Data.Instance.let.Select(a => a.VremePolaska);
            if (opcija.Equals(Opcija.izmena))
            {
                TxtSifra.IsReadOnly = true;
                cbAviokompanija.IsReadOnly = true;
            }
        }

        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            if (opcija.Equals(Opcija.Dodavanje) && validacija()==true)
            {
                let.Aktivan = true;
                let.Lista = new List<Sedista>
                {
                    new Sedista{ Ime = "Red1M1",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M2",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M3",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M4",
                                Zauzeto = false},
                  /*  new Sedista{ Ime = "Red1M5",
                                Zauzeto = false},
                    new Sedista{ Ime = "Red1M6",
                                Zauzeto = false}*/

                };
                let.UnsULetove();

            }
        }

        private void Odustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

        }
        public bool validacija()
        {
            if (TxtSifra.Text.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti sifru");
                return false;
            }
            if ( dpDolazak.Text.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti datum dolaska");
                return false;
            }
            if (dpPolazak.Text.Equals(string.Empty))
            {
                MessageBox.Show("Morate uneti datum polaska");
                return false;
            }
            if (cbAviokompanija.SelectedItem==null)
            {
                MessageBox.Show("Morate uneti aviokompaniju");
                return false;
            }
            if (cbOdrediste.SelectedItem==null)
            {
                MessageBox.Show("Morate uneti odrediste");
                return false;
            }
            if (cbPolaziste.SelectedItem==null)
            {
                MessageBox.Show("Morate uneti polaziste");
                return false;
            }
            try
            {
                Convert.ToDouble(TxtCena.Text);
            }
            catch (Exception e)
            {
                MessageBox.Show("Cena mora biti broj" + e.Message);
                return false;

            }
            if(Convert.ToDouble(TxtCena.Text) <= 0)
            {
                MessageBox.Show("Cena mora biti broj veci od nule");
                return false;
            }
            return true;
        }
    }
}
