﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for BiznisW.xaml
    /// </summary>
    public partial class BiznisW : Window
    {
        Karta karta;
        Let let;

        List<List<Sedista>> lsts = new List<List<Sedista>>();
        List<List<Sedista>> lsts1 = new List<List<Sedista>>();


        public BiznisW(Let let,Karta karta)
        {

            this.karta = karta;
            this.let = let;
            var tCount = lsts.Count;

          
          
         

            for (int i = 0; i < 1; i++)
            {
                lsts.Add(new List<Sedista>());

                for (int j = 0; j < let.Lista.Count; j++)
                {
                    if (let.Lista[j].Zauzeto == false)
                        lsts[i].Add(let.Lista[j]);

                }
            }
            for (int i = 0; i < 1; i++)
            {
                lsts1.Add(new List<Sedista>());

                for (int j = 0; j < let.Lista2.Count; j++)
                {
                    if (let.Lista2[j].Zauzeto == false) 
                        lsts1[i].Add(let.Lista2[j]);


                    }
            }

          

            InitializeComponent();
            DataContext = karta;
            lst.Visibility = Visibility.Hidden;
            lst2.Visibility = Visibility.Hidden;
            karta.BrojLeta = let.Sifra;
            txtBlock.TextWrapping= TextWrapping.Wrap;

            
            
            if (!(Data.Instance.Ulogova.Equals(string.Empty)))
            {
                karta.Putnik = Data.Instance.Ulogova;
                txtPutnik.Visibility = Visibility.Hidden;
                lblPutnik.Content= Data.Instance.Ulogova;
            }


            lst.ItemsSource = lsts;
            lst2.ItemsSource = lsts1;
        }

       

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;

            for (int i= 0; i < let.Lista2.Count;i++)
                {
                    if (let.Lista2[i].Ime.Equals(b.Content.ToString()))
                    {
                        let.Lista2.Remove(let.Lista2[i]);
                    }
                }
            for (int i = 0; i < let.Lista.Count; i++)
            {
                if (let.Lista[i].Ime.Equals(b.Content.ToString()))
                {
                    let.Lista.Remove(let.Lista[i]);
                }
            }


            MessageBox.Show(b.Content.ToString());
            b.Visibility= Visibility.Hidden;
            karta.BrojSedista = b.Content.ToString();


        }

        private void BtnBiznis_Click(object sender, RoutedEventArgs e)
        {
            lst.Visibility = Visibility.Visible;
            btnEkonomska.Visibility = Visibility.Hidden;
            lbKlasa.Content = "Biznis Klasa";
            karta.Klasa = "Biznis";
            karta.Cena = (int)let.Cena + (int)let.Cena/2;
        }

        private void BtnEkonomska_Click(object sender, RoutedEventArgs e)
        {
            lst2.Visibility = Visibility.Visible;
            btnEkonomska.Visibility = Visibility.Hidden;
            lbKlasa.Content = "Ekonomska Klasa";
            karta.Klasa = "Ekonomska";
            karta.Cena = (int)let.Cena;
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            if (validacija() == true)
            {
                karta.Aktivan = true;
                
                MessageBox.Show("Uspesno ste dodali kartu: Korisnik:" + karta.Putnik + "brojSedista:" + karta.BrojSedista + "Let:" + karta.BrojLeta + "cena:" + karta.Cena);
                karta.UnsUAerodrome();
            }
                
        }

        private void BtnOtkazi_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            
        }
        public bool validacija()
        {
            if (karta.Klasa.Equals(string.Empty))
            {
                MessageBox.Show("Nije izabrana klasa");
                return false;
            }
            if (karta.Putnik.Equals(string.Empty))
            {
                MessageBox.Show("Nije dodat putnik");
                return false;
            }
            if (karta.BrojSedista.Equals(string.Empty))
            {
                MessageBox.Show("Nije izabrano sediste");
                return false;
            }
            return true;
        }
    }
}   

