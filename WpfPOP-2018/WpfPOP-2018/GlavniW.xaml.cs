﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for GlavniW.xaml
    /// </summary>
    public partial class GlavniW : Window
    {
        ICollectionView view;
        public GlavniW()
        {
            InitializeComponent();

            view = CollectionViewSource.GetDefaultView(Data.Instance.let);
            Dglet.ItemsSource = Data.Instance.let;

            view.Filter = CustomFilter;

            Dglet.IsSynchronizedWithCurrentItem = true;
            Dglet.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            Dglet.SelectedIndex = 0;
        }

        private void BtnKarta_Click(object sender, RoutedEventArgs e)
        {
            Let selektovanLet = (Let)Dglet.SelectedItem;
            if (selektovanLet != null)
            {

                BiznisW wLet = new BiznisW(selektovanLet, new Karta());
                wLet.ShowDialog();

            }
            else
            {
                MessageBox.Show("Morate izabrati za koji let kupujete kartu");
            }
        }
        private bool CustomFilter(object obj)
        {
            Let let = obj as Let;
            if (txtSearch.Equals(String.Empty))
            {
                return let.Aktivan;
            }
            else
            {
                return let.Aktivan && let.Sifra.Contains(txtSearch.Text) || let.Destinacija.Contains(txtSearch.Text) || let.Odrediste.Contains(txtSearch.Text);
            }
        }
        private void BtnPolazak_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("Odrediste", ListSortDirection.Ascending));
        }

        private void BtnDolazak_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("Destinacija", ListSortDirection.Ascending));
        }

        private void BtnIme_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("Sifra", ListSortDirection.Ascending));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RegistracijaW s = new RegistracijaW(new Model.Korisnik(), RegistracijaW.Opcija.Registracija);
            s.ShowDialog();
        }

        private void dgFakulteti_AutoGeneratingColumn(object sender,
         DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Aktivan" || (string)e.Column.Header == "Lista" || (string)e.Column.Header == "Lista2")
            {
                e.Cancel = true;
            }
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            LogovanjeW g = new LogovanjeW();
            g.ShowDialog();
        }
        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
