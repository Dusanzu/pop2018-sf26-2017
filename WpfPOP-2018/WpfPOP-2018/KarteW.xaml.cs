﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfPOP_2018.Database;
using WpfPOP_2018.Model;

namespace WpfPOP_2018
{
    /// <summary>
    /// Interaction logic for KarteW.xaml
    /// </summary>
    public partial class KarteW : Window
    {
        private ICollectionView view;

        public KarteW()
        {
            InitializeComponent();




            view = CollectionViewSource.GetDefaultView(Data.Instance.karte);
            view.Filter = CustomFilter;

            foreach (Korisnik korisnik in Data.Instance.korisnici)
            {
                if (korisnik.KorisnickoIme.Equals(Data.Instance.Ulogova) && korisnik.TipKorisnka.Equals("Putnik"))
                {
                    BtnKarta.Visibility = Visibility.Hidden;
                    BtnIzmeni.Visibility = Visibility.Hidden;
                    BtnObrisi.Visibility = Visibility.Hidden;
                }
               
            }




            DGKarte.ItemsSource = Data.Instance.karte;
            DGKarte.IsSynchronizedWithCurrentItem = true;
            DGKarte.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private bool CustomFilter(object obj)
        {
           Karta aerodrom = obj as Karta;
         
        
             if (TxtIme.Equals(String.Empty))
            {
                return aerodrom.Aktivan;
            }
            else
            {
                return aerodrom.Aktivan && aerodrom.Putnik.Contains(TxtIme.Text);
            }
        
    }
        private void DGKarte_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        

        private void BtnKarta_Click(object sender, RoutedEventArgs e)
        {
            IzmenaKarteW karta = new IzmenaKarteW(new Karta());
            karta.ShowDialog();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Karta selektovanaKarta = (Karta)DGKarte.SelectedItem;
            if (selektovanaKarta != null)
            {
                Karta stari = (Karta)selektovanaKarta.Clone();
                IzmenaKarteW wAerodrom = new IzmenaKarteW(selektovanaKarta, IzmenaKarteW.Opcija.Izmena);
                if (wAerodrom.ShowDialog() != true || selektovanaKarta.Cena<=0)
                {
                    int indeks = IndexKarte(selektovanaKarta.Putnik);
                    Data.Instance.karte[indeks] = stari;
                }

            }
            else
            {
                MessageBox.Show("Nije selektovan ni jedan aerodrom");
            }
            selektovanaKarta.Izmena();
            DGKarte.Items.Refresh();

        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Karta karta = (Karta)DGKarte.SelectedItem;



            if (Selektovan(karta))
            {
                if (MessageBox.Show("Da li ste sigurni", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    var index = IndexKarte(karta.Putnik);
                    Data.Instance.karte[index].Aktivan = false;
                    Karta selektovanaKarta = (Karta)DGKarte.SelectedItem;
                    selektovanaKarta.Izmena();

                }
            }
            view.Refresh();

        }
        public int IndexKarte(string sifra)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.karte.Count; i++)
            {
                if (Data.Instance.karte[i].Putnik.Equals(sifra))
                {

                    index = i;
                    break;
                }
            }
            return index;

        }
        public bool Selektovan(Karta karta)
        {
            if (karta == null)
            {
                MessageBox.Show("Morate izabrati kartu iz liste");
                return false;
            }
            return true;
        }

        private void BtnSifra_Click(object sender, RoutedEventArgs e)
        {
            view.SortDescriptions.Add(new SortDescription("Sifra", ListSortDirection.Ascending));
        }

        private void TxtIme_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
        private void dgFakulteti_AutoGeneratingColumn(object sender,
        DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "Aktivan" /*|| (string)e.Column.Header == "Aktivan"*/)
            {
                e.Cancel = true;
            }
        }
    }
}
